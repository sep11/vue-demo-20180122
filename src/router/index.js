import Vue from 'vue'
import Router from 'vue-router'
import main from '@/page/main.vue'
import login from '@/page/login.vue'
import home from '@/page/home.vue'
import find from '@/page/find.vue'
import setting from '@/page/setting'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',//传递值？
      component: login,
      alias: '/home'
    },
    {
      path: '/main',
      name: 'main',
      component: main,
      children: [
        {
          path: 'home',
          component: home
        }, {
          path: 'find',
          component: find
        }, {
          path: 'setting',
          component: setting
        }
      ]
    }
  ]
})




