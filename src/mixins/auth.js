export default {
  data() {
    return {
      auth: null
    }
  },
  created() {
    this.getAuth()
  },
  methods: {
    getAuth() {
      this.auth = {
        token: '110',
        roles: ['admin']
      }
    }
  }
}
