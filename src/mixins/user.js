export default {
  data() {
    return {
      userInfo: null
    }
  },
  created() {
    this.getUserInfo()
  },
  methods: {
    getUserInfo() {
      this.userInfo = {
        name: 'susan',
        pwd: '123456',
        token: '110'
      }
    }
  }
}
